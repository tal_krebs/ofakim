﻿import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FetchEmployeeComponent } from '../fetchemployee/fetchemployee.component';
import { EmployeeService } from '../../services/empservice.service';

@Component({
    selector: 'createemployee',
    templateUrl: './AddEmployee.component.html'
})

export class createemployee implements OnInit {
    employeeForm: FormGroup;
    title: string = "Create";
    id: number;
    errorMessage: any;
    nameVal: string = "";
    genderVal: string = "";
    emailVal: string = "";
    phoneNumberVal: string = "";
    birthdayVal: string = "";

    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
        private _employeeService: EmployeeService, private _router: Router) {
        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }

        this.employeeForm = this._fb.group({
            id: 0,
            name: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            email: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required]],
            birthday: ['', [Validators.required]]
        })
    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._employeeService.getEmployeeById(this.id)
                .subscribe(resp => this.employeeForm.setValue(resp)
                , error => this.errorMessage = error);
        }
        debugger;
        let employee: any = localStorage.getItem("emp");
        if (employee) {
            try {
                employee = JSON.parse(employee);
                this.nameVal = employee.name;
                this.genderVal = employee.gender;
                this.emailVal = employee.email;
                this.phoneNumberVal = employee.phoneNumber;
                this.birthdayVal = employee.birthday;
            } catch (e) {
                console.log("error:" + e);
            }
            finally {
                localStorage.removeItem("emp");// remove the item after we save it to db.
            }
            
        }
    }

    save() {

        if (!this.employeeForm.valid) {
            return;
        }

        if (this.title == "Create") {
            this._employeeService.saveEmployee(this.employeeForm.value)
                .subscribe((data) => {
                    this._router.navigate(['/fetch-employee']);
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._employeeService.updateEmployee(this.employeeForm.value)
                .subscribe((data) => {
                    this._router.navigate(['/fetch-employee']);
                }, error => this.errorMessage = error) 
        }
    }

    cancel() {
        this._router.navigate(['/fetch-employee']);
    }

    get name() { return this.employeeForm.get('name'); }
    get gender() { return this.employeeForm.get('gender'); }
    get email() { return this.employeeForm.get('email'); }
    get phoneNumber() { return this.employeeForm.get('phoneNumber'); }
    get birthday() { return this.employeeForm.get('birthday'); }
}