﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class EmployeeService {
    myAppUrl: string = "";
    emp: any = ""; // save the emp object

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }

    getEmployees() {
        return this._http.get(this.myAppUrl + 'api/Employee/Index')
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    getEmployeeById(id: number) {
        return this._http.get(this.myAppUrl + "api/Employee/Details/" + id)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }

    saveEmployee(employee) {
        this.emp = employee;
        console.log("employee:" + this.emp);
        return this._http.post(this.myAppUrl + 'api/Employee/Create', employee)
            .map((response: Response) => {
                debugger;
                //bonus next time the user will click on "create new" the form will be full.
                if (response.json() === -1) { // if we got a error from the server then we save to local storage. 
                    if (this.emp !== "") {
                        localStorage.setItem("emp", JSON.stringify(this.emp)); // save the employee object to local storage.
                    }
                }
                //response.json();
            })
            .catch(this.errorHandler);
    }

    updateEmployee(employee) {
        return this._http.put(this.myAppUrl + 'api/Employee/Edit', employee)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    deleteEmployee(id) {
        return this._http.delete(this.myAppUrl + "api/Employee/Delete/" + id)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        debugger;
        console.log(error);
        console.log("save to local storage");
        if (this.emp !== "") {
            localStorage.setItem("emp", JSON.stringify(this.emp));
        }
        return Observable.throw(error);
    }
}